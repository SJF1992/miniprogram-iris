//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    show: true,
    current:"home",
    tabBar:{},  // 自定义taBar
    source:[],
    styleOptions:{
      color: "#333",
      backgroundColor: "#f1f1f1"
    },
     list: [
      {
        path: "/pages/home/home",
        name: "首页",
        icon: "iconfont icon-shouye",
        index: "1",
        id: "home",
        src:"../home/home.wxml"
      },
      {
        path: "/pages/class/class",
        name: "分类",
        icon: "iconfont icon-fenlei",
        index: "2",
        id: "class",
        src: "/class/class.wxml"
      },
      {
        path: "/pages/cart/cart",
        name: "购物车",
        icon: "iconfont icon-gouwuche",
        index: "3",
        id: "cart",
        src: "/cart/cart.wxml"
      }
    ],
  },
  onLoad: function () {
    var tab = this.selectComponent("#custom_tabBar")
    this.setData({
      tabBar: tab
    })
    // 组件中的数据源
    console.log(this.data.tabBar.data.list)
    this.setData({
      source: tab.data.list
    })
  },
  switchTab: function (e) {
    let path = e.detail.path
    let title = e.detail.name
    var currentName = e.detail.id

    this.setData({
      current: currentName
    })
    wx.setNavigationBarTitle({
      title: title,
    })
  }
})
