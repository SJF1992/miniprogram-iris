// pages/ex/index.wxml.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: "数据绑定",
    name: "Iris",
    show: true,
    arr:[{name:"jeff"},{name:"Iris"}],
    item:{
      msg:"這是個模板"
    },
    wxsTest:"我就试一下",
    compontentText:"我是从page过来的",
    fromComponents:"组件传值"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  componentsClick: function(e){
    console.log(e.detail.name)
    this.setData({
      fromComponents: e.detail.name
    })
  }
})