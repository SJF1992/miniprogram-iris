// components/tabBar/tabBar.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    styleOptions:{
      type: Object,
      value: {
        color: "#333",
        backgroundColor: "#f1f1f1"
      }
    },
    list:{
      type: Array,
      value: [
        {
          path: "/pages/home/home",
          name: "首页",
          icon: "iconfont icon-shouye",
          index: "1",
          id: "home",
          src: "../home/home.wxml"
        },
        {
          path: "/pages/class/class",
          name: "分类",
          icon: "iconfont icon-fenlei",
          index: "2",
          id: "class",
          src: "/class/class.wxml"
        },
        {
          path: "/pages/cart/cart",
          name: "购物车",
          icon: "iconfont icon-gouwuche",
          index: "3",
          id: "cart",
          src: "/cart/cart.wxml"
        }, {
          path: "/pages/mine/mine",
          name: "我的",
          icon: "iconfont icon-wode",
          index: "4",
          id: "mine",
          src: "/mine/mine.wxml"
        }
      ]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // list: [
    //   {
    //     path: "/pages/home/home",
    //     name: "首页",
    //     icon: "iconfont icon-shouye",
    //     index: "1",
    //     id: "home",
    //     src:"../home/home.wxml"
    //   },
    //   {
    //     path: "/pages/class/class",
    //     name: "分类",
    //     icon: "iconfont icon-fenlei",
    //     index: "2",
    //     id: "class",
    //     src: "/class/class.wxml"
    //   },
    //   {
    //     path: "/pages/cart/cart",
    //     name: "购物车",
    //     icon: "iconfont icon-gouwuche",
    //     index: "3",
    //     id: "cart",
    //     src: "/cart/cart.wxml"
    //   }, {
    //     path: "/pages/mine/mine",
    //     name: "我的",
    //     icon: "iconfont icon-wode",
    //     index: "4",
    //     id: "mine",
    //     src: "/mine/mine.wxml"
    //   }
    // ],
    activeIndex: 1,
    styleStr: ""
  },

  attached: function () {
    let pages = getCurrentPages()   // 获取
    let currentPage = pages[pages.length - 1]
    let currentRoute = currentPage.route
    this.handleStyle()
  },

  /**
   * 组件的方法列表
   */
  methods: {
    switchTab: function (e) {
      let item = e.currentTarget.dataset.item
      this.setData({
        activeIndex: e.currentTarget.dataset.item.index
      })
      this.triggerEvent('tabClick', item)
    },
    handleStyle(){
      var str = ""
      for (var key in this.properties.styleOptions) {
        str += `${key}:${this.properties.styleOptions[key]};`
      }
      this.setData({
        styleStr: str
      })
    }
  }
})
